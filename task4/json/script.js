// 1. преобразовать объект в строку
let user = {
    name: "Mary",
    yearOfBirth: 1992,
    role: "admin",
    permission: "write, read, update, delete", 
}

let string = JSON.stringify(user)

console.log(string)

// 2. полученные данные преобразовать в обьект

console.log(JSON.parse(string))

// 3. Добавить метод toJSON в объект пользователя и сделать вывод только name и
// yearOfBirth во время преобразования методом JSON.stringify и выполнить преобразовании
// и проверить.
let user2 = {
    name: "Mary",
    yearOfBirth: 1992,
    role: "admin",
    permission: "write, read, update, delete",
    toJson() {
        return `name: ${this.name}, yearOfBirth: ${this.yearOfBirth}`;
    }
}

console.log(user2.toJson())

// 4. Создать форму с полями Username, Email и Message. При нажатии на кнопку Submit
// собрать данные с полей формы и вывести в консоль данные в формате JSON.

const form = document.getElementById("dev-form")


form.addEventListener("submit", (e) => {
    e.preventDefault();

    
    const inputs = document.querySelectorAll("input, textarea");
    
    const getcheckboxes = (name) => {
        return Array.from(form.querySelectorAll("input[name='${name}']"))
        .filter(checkbox => checkbox.checked)
        .map(checkbox => checkbox.value);
    }

    const formObject = Array.from(inputs).reduce((obj, field) => {
       obj[field.name] = field.type === "checkbox" ? getcheckboxes(field.name) : field.value;

       return obj;
    }, {});

    console.log(JSON.stringify(formObject));
    // console.log(JSON.(formObject));
});